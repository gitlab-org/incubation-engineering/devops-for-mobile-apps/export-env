# frozen_string_literal: true

require "thor"

module ExportEnv
  class Exporter
    def initialize(file)
      @file = file || 'exported.env'
    end

    def process!
      raise "#{@file} already exists, exiting" if File.exists?(@file)

      File.open(@file, "w") { |f| 
        ENV.each do |env_var|
          f.write "export #{env_var[0]}=\"#{env_var[1]}\"\n"
        end
      }

      return "Environment exported to #{@file}"
    end
  end
end
