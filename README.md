# ExportEnv

Export a local environment to a source-able file

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add export_env

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install export_env

## Usage

Export current environment:

```
export_env export
Environment exported to exported.env
```

Load environment from file:

```
source exported.env
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the ExportEnv project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/export_env/blob/master/CODE_OF_CONDUCT.md).
