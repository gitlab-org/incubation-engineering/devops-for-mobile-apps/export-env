# frozen_string_literal: true

require_relative "export_env/version"

module ExportEnv
  class Error < StandardError; end
  # Your code goes here...
end
