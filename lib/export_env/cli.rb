# frozen_string_literal: true

require "thor"

module ExportEnv
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    desc "version", "export_env version"
    def version
      require_relative "version"
      puts "v#{ExportEnv::VERSION}"
    end
    map %w[--version -v] => :version

    desc "export FILE", "exports the current ENV to a source-able file at the given location"
    def export(file = nil)
      require_relative "exporter"  
      puts ExportEnv::Exporter.new(file).process!
    end
  end
end
